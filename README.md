# OpenIPMC-HW_debug-base

PCB intended to program, develop, and debug OpenIPMC-HW.

## License

OpenIPMC-HW is Copyright 2020-2021 of Luis Ardila-Perez. Luigi Calligaris, and Andre Cascadan. OpenIPMC-HW is released under "CERN Open Hardware Licence Version 2 - Permissive" license. Please refer to the LICENSE document included in this repository.

